# RPG-Tools
Short utility scripts for RPGs

## What is this?
This is going to be a collection of the small script I wrote for various purposes for developing and fiddling with RPGs. Mostly utilities like generating XP curves or names and the like.

### What's in here?

#### xpCurve.py
This script generates an XP curve for levels, with a user-specified base XP and exponent. Pretty easy to use, just set the variables as you want and fire it. Outputs to a text file.
