# Simple script to caclulate a level curve for RPGs
# Takes a base level XP amount, adds an exponent to that, prints to file

baseXP = 100 # Base XP for level 1
genLevel = 100 # Number of levels to generate
exponent = 0.2 # Exponent amount
levelXP = baseXP # Starting XP for level 1
output = open('lvlOutput.txt', 'w') # Write level curves to a file
for x in range(genLevel): # For every level until the max
    levelXP = levelXP + (levelXP * exponent) # Calculate the XP needed to reach this level
    output.write('Level ' + str(x+1) + ': ' + str(int(levelXP)) + '\n')
output.close() # Close up the file
